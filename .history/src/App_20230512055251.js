import React, { useEffect } from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { ACCESS_TOKEN } from './services/config';
import { fetchMe } from './redux/actions/authAction';
import { AuthRoute, PrivateRoute } from './HOCs/route/rootRoute';
import DrawerModalPopup from './HOCs/DrawerModalPopup';
import { createBrowserHistory } from 'history';
// layouts
import AuthLayout from './HOCs/layouts/AuthLayout/AuthLayout';
import MainLayout from './HOCs/layouts/MainLayout/MainLayout';
import SigninPage from './pages/SigninPage/SigninPage';
import SignupPage from './pages/SignupPage/SignupPage';
import ProjectPage from './pages/ProjectPage/ProjectPage';
import NewProjectPage from './pages/ProjectPage/NewProjectPage/NewProjectPage';
import DetailProjectPage from './pages/ProjectPage/DetailProjectPage/DetailProjectPage';
import EditProjectPage from './pages/ProjectPage/EditProjectPage/EditProjectPage';
import TaskPage from './pages/TaskPage/TaskPage';
import MyProfilePage from './pages/MyProfilePage/MyProfilePage';
import UserManagementPage from './pages/UserManagementPage/UserManagementPage';
import NotFoundPage from './pages/NotFoundPage/NotFoundPage';

// pages
export const history = createBrowserHistory();

const App = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    const token = localStorage.getItem(ACCESS_TOKEN);
    if (token) dispatch(fetchMe);
  }, [dispatch]);

  return (
    <BrowserRouter history={history}>
      <DrawerModalPopup />

      <Routes>
        <AuthRoute
          path="/signin"
          exact
          element={SigninPage}
          redirectPath="/projects"
          layout={AuthLayout}
        />
        <AuthRoute path="/register" exact element={SignupPage} redirectPath="/projects" />
        <PrivateRoute
          path="/projects"
          exact
          element={ProjectPage}
          layout={MainLayout}
          redirectPath="/signin"
        />
        <PrivateRoute
          path="/projects/new"
          exact
          element={NewProjectPage}
          layout={MainLayout}
          redirectPath="/signin"
        />
        <PrivateRoute
          path="/projects/:id"
          exact
          element={DetailProjectPage}
          layout={MainLayout}
          redirectPath="/signin"
        />
        <PrivateRoute
          path="/projects/:id/edit"
          exact
          element={EditProjectPage}
          layout={MainLayout}
          redirectPath="/signin"
        />
        <PrivateRoute
          path="/projects/:projectId/board"
          exact
          element={TaskPage}
          layout={MainLayout}
          redirectPath="/signin"
        />
        <PrivateRoute
          path="/users"
          exact
          element={UserManagementPage}
          layout={MainLayout}
          redirectPath="/signin"
        />
        <PrivateRoute
          path="/my-profile"
          exact
          element={MyProfilePage}
          layout={MainLayout}
          redirectPath="/signin"
        />
        <Route path="/" exact element={SigninPage} />

        <PrivateRoute path="*" element={NotFoundPage} layout={MainLayout} redirectPath="/signin" />
      </Routes>
    </BrowserRouter>
  );
};

export default App;
