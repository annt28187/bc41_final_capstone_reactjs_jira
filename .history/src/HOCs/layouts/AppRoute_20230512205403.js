import React from 'react';
import { useSelector } from 'react-redux';
import { Navigate } from 'react-router-dom';

const RouteComponent = ({ isAuth, Component, redirectPath, isAdmin }) => {
  const { infoUser } = useSelector((state) => state.reducer);
  const token = localStorage.getItem('USER_TOKEN');

  if (isAdmin) return token ? <Component /> : <Navigate to={redirectPath} />;
  return <Component />;
};

export default RouteComponent;
