import React from 'react';
import Header from './Header/Header';
import { Route } from 'react-router-dom';

export default function MainLayout(props) {
  const { Comp, ...restParam } = props;
  return <Route {...restParam} render={routeProps=>{
    return (
    <>
      <Header />
      <main className="container py-6"><Comp {...routeProps}/></main>
    </>
  );
  }}
}
