import React from 'react';
import Header from './Header';

export default function MainLayout() {
  return (
    <>
      <Header />
      <main className="container py-6">{props.children}</main>
    </>
  );
}
