import React from 'react';
import Header from './Header/Header';

export default function MainLayout({ Component }) {
  return (
    <>
      <Header />
      <main className="container py-6">
        <Component />
      </main>
    </>
  );
}
