import React from 'react';
import Header from './Header/Header';

export default function MainLayout(props) {
  return (
    <>
      <Header />
      <main className="container py-6">{props.children}</main>
    </>
  );
}
