import React from 'react';
import Header from './Header/Header';
import { Route } from 'react-router-dom';

export default function MainLayout({ Component }) {
  return (
    <>
      <Header />
      <main className="container py-6">
        <Component />
      </main>
    </>
  );
}
