import React from 'react';
import { Col, Row } from 'antd';
import { ReactComponent as LoginPic } from '../../../assets/images/login/panel.svg';
import { Route } from 'react-router-dom';
import { render } from '@testing-library/react';

export default function AuthLayout(props) {
  const { Comp, ...restParam } = props;
  return (
    <Route
      {...restParam}
      render={(routeProps) => {
        return (
          <>
            <Row>
              <Col xs={0} md={12} lg={14} style={{ backgroundColor: '#001529' }}>
                <LoginPic className="mx-auto w-8/12 h-full" />
              </Col>
              <Col xs={24} md={12} lg={10}>
                {props.children}
              </Col>
            </Row>
          </>
        );
      }}
    />
  );
}
