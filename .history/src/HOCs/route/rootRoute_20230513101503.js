import MyProfilePage from '../../pages/MyProfilePage/MyProfilePage';
import DetailProjectPage from '../../pages/ProjectPage/DetailProjectPage/DetailProjectPage';
import EditProjectPage from '../../pages/ProjectPage/EditProjectPage/EditProjectPage';
import NewProjectPage from '../../pages/ProjectPage/NewProjectPage/NewProjectPage';
import ProjectPage from '../../pages/ProjectPage/ProjectPage';
import SigninPage from '../../pages/SigninPage/SigninPage';
import SignupPage from '../../pages/SignupPage/SignupPage';
import TaskPage from '../../pages/TaskPage/TaskPage';
import UserManagementPage from '../../pages/UserManagementPage/UserManagementPage';
import AuthLayout from '../layouts/AuthLayout/AuthLayout';
import MainLayout from '../layouts/MainLayout/MainLayout';

export const rootRoute = [
  { path: '/', component: SigninPage, redirectPath: '/projects', layout: AuthLayout },
  { path: '/signin', component: SigninPage, redirectPath: '/projects', layout: MainLayout },
  { path: '/signup', component: SignupPage, redirectPath: '/projects', layout: MainLayout },
  {
    path: '/projects',
    component: ProjectPage,
    isAdmin: true,
    redirectPath: '/signin',
    layout: MainLayout,
  },
  {
    path: '/projects/:id',
    component: DetailProjectPage,
    isAdmin: true,
    redirectPath: '/signin',
    layout: MainLayout,
  },
  {
    path: '/projects/new',
    component: NewProjectPage,
    isAdmin: true,
    redirectPath: '/signin',
    layout: MainLayout,
  },
  {
    path: '/projects/:id/edit',
    component: EditProjectPage,
    isAdmin: true,
    redirectPath: '/signin',
    layout: MainLayout,
  },
  {
    path: '/projects/:projectId/board',
    component: TaskPage,
    isAdmin: true,
    redirectPath: '/signin',
    layout: MainLayout,
  },
  {
    path: '/users',
    component: UserManagementPage,
    isAdmin: true,
    redirectPath: '/signin',
    layout: MainLayout,
  },
  {
    path: 'my-profile',
    component: MyProfilePage,
    isAdmin: true,
    redirectPath: '/signin',
    layout: MainLayout,
  },
];
