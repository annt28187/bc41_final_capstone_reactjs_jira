import MyProfilePage from '../../pages/MyProfilePage/MyProfilePage';
import DetailProjectPage from '../../pages/ProjectPage/DetailProjectPage/DetailProjectPage';
import EditProjectPage from '../../pages/ProjectPage/EditProjectPage/EditProjectPage';
import NewProjectPage from '../../pages/ProjectPage/NewProjectPage/NewProjectPage';
import ProjectPage from '../../pages/ProjectPage/ProjectPage';
import SigninPage from '../../pages/SigninPage/SigninPage';
import SignupPage from '../../pages/SignupPage/SignupPage';
import TaskPage from '../../pages/TaskPage/TaskPage';
import UserManagementPage from '../../pages/UserManagementPage/UserManagementPage';

export const rootRoute = [
  { path: '/', component: SigninPage },
  { path: '/signin', component: SigninPage },
  { path: '/signup', component: SignupPage },
  { path: '/project', component: ProjectPage, isAdmin: true, redirectPath: '/signin' },
  {
    path: '/project/detail/:id',
    component: DetailProjectPage,
    isAdmin: true,
    redirectPath: '/signin',
  },
  {
    path: '/project/new',
    component: NewProjectPage,
    isAdmin: true,
    redirectPath: '/signin',
  },
  {
    path: '/project/:id/edit',
    component: EditProjectPage,
    isAdmin: true,
    redirectPath: '/signin',
  },
  {
    path: '/project/:projectId/board',
    component: TaskPage,
    isAdmin: true,
    redirectPath: '/signin',
  },
  {
    path: '/user',
    component: UserManagementPage,
    isAdmin: true,
    redirectPath: '/signin',
  },
  {
    path: 'my-profile',
    component: MyProfilePage,
    isAdmin: true,
    redirectPath: '/signin',
  },
];
