import MyProfilePage from '../../pages/MyProfilePage/MyProfilePage';
import DetailProjectPage from '../../pages/ProjectPage/DetailProjectPage/DetailProjectPage';
import EditProjectPage from '../../pages/ProjectPage/EditProjectPage/EditProjectPage';
import NewProjectPage from '../../pages/ProjectPage/NewProjectPage/NewProjectPage';
import ProjectPage from '../../pages/ProjectPage/ProjectPage';
import SigninPage from '../../pages/SigninPage/SigninPage';
import SignupPage from '../../pages/SignupPage/SignupPage';
import TaskPage from '../../pages/TaskPage/TaskPage';
import UserManagementPage from '../../pages/UserManagementPage/UserManagementPage';

export const rootRoute = [
  { path: '/', component: SigninPage, redirectPath: '/projects' },
  { path: '/signin', component: SigninPage, redirectPath: '/projects' },
  { path: '/signup', component: SignupPage, redirectPath: '/projects' },
  { path: '/projects', component: ProjectPage, isAdmin: true, redirectPath: '/signin' },
  {
    path: '/projects/:id',
    component: DetailProjectPage,
    isAdmin: true,
    redirectPath: '/signin',
  },
  {
    path: '/projects/new',
    component: NewProjectPage,
    isAdmin: true,
    redirectPath: '/signin',
  },
  {
    path: '/projects/:id/edit',
    component: EditProjectPage,
    isAdmin: true,
    redirectPath: '/signin',
  },
  {
    path: '/projects/:projectId/board',
    component: TaskPage,
    isAdmin: true,
    redirectPath: '/signin',
  },
  {
    path: '/users',
    component: UserManagementPage,
    isAdmin: true,
    redirectPath: '/signin',
  },
  {
    path: 'my-profile',
    component: MyProfilePage,
    isAdmin: true,
    redirectPath: '/signin',
  },
];
