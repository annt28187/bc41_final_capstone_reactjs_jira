import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import { ACCESS_TOKEN } from '../../services/config';

const rootRoute = (condition) => {
  return ({
    path,
    component: RouteComponent,
    layout: LayoutComponent,
    redirectPath,
    ...restProps
  }) => {
    return (
      <Route
        path={path}
        {...restProps}
        render={(routeProps) => {
          if (condition()) {
            if (LayoutComponent) {
              return (
                <LayoutComponent>
                  <RouteComponent {...routeProps} />
                </LayoutComponent>
              );
            }

            return <RouteComponent {...routeProps} />;
          }
          return <Redirect to={redirectPath} />;
        }}
      />
    );
  };
};

export const AuthRoute = rootRoute(() => !localStorage.getItem(ACCESS_TOKEN));

export const PrivateRoute = rootRoute(() => localStorage.getItem(ACCESS_TOKEN));
