import React, { useEffect } from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { ACCESS_TOKEN } from './services/config';
import { fetchMe } from './redux/actions/authAction';
import { rootRoute } from './HOCs/route/rootRoute';
import DrawerModalPopup from './HOCs/DrawerModalPopup';
import { createBrowserHistory } from 'history';
// layouts
import AuthLayout from './HOCs/layouts/AuthLayout/AuthLayout';
import MainLayout from './HOCs/layouts/MainLayout/MainLayout';
import SigninPage from './pages/SigninPage/SigninPage';
import SignupPage from './pages/SignupPage/SignupPage';
import ProjectPage from './pages/ProjectPage/ProjectPage';
import NewProjectPage from './pages/ProjectPage/NewProjectPage/NewProjectPage';
import DetailProjectPage from './pages/ProjectPage/DetailProjectPage/DetailProjectPage';
import EditProjectPage from './pages/ProjectPage/EditProjectPage/EditProjectPage';
import TaskPage from './pages/TaskPage/TaskPage';
import MyProfilePage from './pages/MyProfilePage/MyProfilePage';
import UserManagementPage from './pages/UserManagementPage/UserManagementPage';
import NotFoundPage from './pages/NotFoundPage/NotFoundPage';
import { useSelector } from 'react-redux';
import RouteComponent from './HOCs/layouts/AppRoute';

// pages
export const history = createBrowserHistory();

function App() {
  return (
    <BrowserRouter>
      <DrawerModalPopup />
      <Routes>
        {rootRoute.map(
          ({ path, component: Component, isPrivate, isAuth, redirectPath, isAdmin, layout }) => (
            <Route
              key={path}
              path={path}
              element={
                <RouteComponent
                  isPrivate={isPrivate}
                  isAdmin={isAdmin}
                  isAuth={isAuth}
                  Component={Component}
                  redirectPath={redirectPath}
                  layout={layout}
                />
              }
            />
          )
        )}
        <Route path="*" element={<NotFoundPage />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
