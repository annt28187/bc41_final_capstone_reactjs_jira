import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { rootRoute } from './HOCs/route/rootRoute';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        {rootRoute.map(({ url, component }, id) => {
          return <Route exact key={id} path={url} element={component} />;
        })}
      </Routes>
    </BrowserRouter>
  );
}

export default App;
