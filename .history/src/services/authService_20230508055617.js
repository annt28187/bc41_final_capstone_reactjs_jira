import axios from 'axios';
import * as yup from 'yup';
import { yupContent } from './yupContent';

export const schemaSignin = yup.object().shape({
  email: yupContent.email,
  password: yupContent.password,
});

export const schemaSignup = yup.object().shape(yupContent);

export const authServ = {
  signup: (loginData) => {
    return axios({
      url: `${BASE_URL}/Users/signin`,
      method: 'POST',
      data: loginData,
      headers: configHeaders(),
    });
  },
  testUserTokenAPI: () => {
    return axios({
      url: `${BASE_URL}/Users/TestToken`,
      method: 'POST',
      headers: configHeaders(),
    });
  },
};
