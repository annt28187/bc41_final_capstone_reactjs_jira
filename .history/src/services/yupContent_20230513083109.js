import * as yup from 'yup';

const regexPassword = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;

const regexPhoneNumber =
  /^(0|84)(2(0[3-9]|1[0-6|8|9]|2[0-2|5-9]|3[2-9]|4[0-9]|5[1|2|4-9]|6[0-3|9]|7[0-7]|8[0-9]|9[0-4|6|7|9])|3[2-9]|5[5|6|8|9]|7[0|6-9]|8[0-6|8|9]|9[0-4|6-9])([0-9]{7})$/;

export const yupSignupContent = {
  email: yup.string().required('Email không để trống'),
  password: yup
    .string()
    .required('Mật khẩu không để trống')
    .min(8, 'Mật khẩu phải có 8-32 ký tự!')
    .max(32, 'Mật khẩu phải có 8-32 ký tự!'),
  // .matches(regexPassword, 'Mật khẩu gồm có chữ hoa, chữ thường và số'),
  name: yup.string().required('Tài khoản không để trống'),
  phoneNumber: yup
    .string()
    .required('Số điện thoại không để trống')
    .matches(regexPhoneNumber, 'Số điện thoại từ 10 - 11 số'),
};

export const yupEditUserContent = {
  email: yup.string().required('Email không để trống'),
  password: yup
    .string()
    .required('Mật khẩu không để trống')
    .min(8, 'Mật khẩu phải có 8-32 ký tự!')
    .max(32, 'Mật khẩu phải có 8-32 ký tự!')
    .matches(regexPassword, 'Mật khẩu gồm có chữ hoa, chữ thường và số'),
  passwordConfirmation: yup
    .string()
    .required('Mật khẩu nhập lại không để trống')
    .oneOf([yup.ref('password')], 'Mật khẩu nhập lại không trùng khớp'),
  name: yup.string().required('Tài khoản không để trống'),
  phoneNumber: yup
    .string()
    .required('Số điện thoại không để trống')
    .matches(regexPhoneNumber, 'Số điện thoại từ 10 - 11 số'),
};

export const yupCreateProjectContent = {
  taskName: yup.string().required('Tên dự án không để trống'),
  categoryId: yup
    .number()
    .required('Danh mục dự án là bắt buộc')
    .min(1, 'Danh mục dự án có 1-3 ký tự!')
    .max(3, 'Danh mục dự án có 1-3 ký tự!'),
};

export const yupCreateTaskContent = {
  taskName: yup.string().required('Tên dự án không để trống'),
  description: yup.string().required('Mô tả không để trống'),
};
