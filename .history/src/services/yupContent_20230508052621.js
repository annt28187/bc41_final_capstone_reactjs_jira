import * as yup from 'yup';

const regexPassword = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;

const regexPhoneNumber = /(84|0[3|5|7|8|9])+([0-9]{8})\b/g;

export const yupContent = {
  email: yup.string().required('Email không để trống').email('Email không hợp lệ'),
  password: yup
    .string()
    .required('Mật khẩu không để trống')
    .min(8, 'Mật khẩu phải có 8-32 ký tự!')
    .max(32, 'Mật khẩu phải có 8-32 ký tự!')
    .matches(regexPassword, 'Mật khẩu gồm có chữ hoa, chữ thường và số'),
  name: yup.string().required('Tài khoản không để trống'),
  phoneNumber: yup.string.required('Số điện thoại không để trống'),
};
