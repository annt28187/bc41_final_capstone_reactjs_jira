export const USER_LOGIN = 'USER_LOGIN';
export const localUserService = {
  get: () => {
    let jsonData = localStorage.getItem(USER_LOGIN);
    return JSON.parse(jsonData);
  },
};
