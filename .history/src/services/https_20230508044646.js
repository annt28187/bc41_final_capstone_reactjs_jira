import axios from 'axios';
import { BASE_URL, ACCESS_TOKEN, TOKEN_CYBERSOFT } from './config';

const https = axios.create({
  baseURL: BASE_URL,
  headers: {
    'content-type': 'application/json',
    TokenCybersoft: TOKEN_CYBERSOFT,
  },
});

https.interceptors.request.use(
  (config) => {
    const accessToken = localStorage.getItem(ACCESS_TOKEN);

    if (accessToken) {
      config.headers.Authorization = 'Bearer ' + accessToken;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

export default https;
