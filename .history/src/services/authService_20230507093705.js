import axios from 'axios';
import { BASE_URL, configHeaders } from './config';

export const authServ = {
  signup: (loginData) => {
    return axios({
      url: `${BASE_URL}/Users/signin`,
      method: 'POST',
      data: loginData,
      headers: configHeaders(),
    });
  },
  testUserTokenAPI: () => {
    return axios({
      url: `${BASE_URL}/Users/TestToken`,
      method: 'POST',
      headers: configHeaders(),
    });
  },
};
