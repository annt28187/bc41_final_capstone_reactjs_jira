import { yupCreateProjectContent } from './yupContent';
import https from './https';
import * as yup from 'yup';

export const schemaCreateProject = yup.object().shape(yupCreateProjectContent);

export const projectService = {
  fetchAllProjects: (params) => {
    return https.get('/Project/getAllProject', { params });
  },
  fetchAllProjectCategories: () => {
    return https.get('/ProjectCategory');
  },
  createProjectAuthorize: (data) => {
    return https.post('/Project/createProjectAuthorize', data);
  },
  updateProject: (data) => {
    return https.put(`/Project/updateProject?projectId=${data.id}`, data);
  },
  fetchUsersByProject: (projectId) => {
    return https.get('/Users/getUserByProjectId', { params: { idProject: projectId } });
  },
  assignUserToProject: (data) => {
    return https.post('/Project/assignUserProject', data);
  },
  removeUserFromProject: (data) => {
    return https.post('/Project/removeUserFromProject', data);
  },
  deleteProject: (projectId) => {
    return https.delete('/Project/deleteProject', { params: { projectId } });
  },
  fetchProjectDetail: (projectId) => {
    return https.get('/Users/getUserByProjectId', { params: { id: projectId } });
  },
};
