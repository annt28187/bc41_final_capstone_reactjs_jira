import axios from 'axios';
import { BASE_URL, ACCESS_TOKEN, TOKEN_CYBERSOFT } from './config';

export const https = axios.create({
  baseURL: BASE_URL,
  headers: {
    'content-type': 'application/json',
    TokenCybersoft: TOKEN_CYBERSOFT,
  },
});
