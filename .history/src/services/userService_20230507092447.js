import axios from 'axios';
import { BASE_URL, configHeaders, https } from './config';

export const userServ = {
  login: (loginData) => {
    return axios({
      url: `${BASE_URL}/QuanLyNguoiDung/DangNhap`,
      method: 'POST',
      data: loginData,
      headers: configHeaders(),
    });
  },
};
