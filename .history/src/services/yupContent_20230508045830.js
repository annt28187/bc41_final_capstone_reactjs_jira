import * as yup from 'yup';

export const yupContent = {
  email: yup.string().required('Email không để trống').email('Email không hợp lệ'),
  password: yup
    .string()
    .required('Mật khẩu không để trống')
    .min(8, 'Mật khẩu phải có 8-32 ký tự!')
    .max(32, 'Mật khẩu phải có 8-32 ký tự!'),
};
