import * as yup from 'yup';

const regexPassword = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;

const regexPhoneNumber =
  /^(0|84)(2(0[3-9]|1[0-6|8|9]|2[0-2|5-9]|3[2-9]|4[0-9]|5[1|2|4-9]|6[0-3|9]|7[0-7]|8[0-9]|9[0-4|6|7|9])|3[2-9]|5[5|6|8|9]|7[0|6-9]|8[0-6|8|9]|9[0-4|6-9])([0-9]{7})$/;

export const yupContent = {
  email: yup.string().required('Email không để trống').email('Email không hợp lệ'),
  password: yup
    .string()
    .required('Mật khẩu không để trống')
    .min(8, 'Mật khẩu phải có 8-32 ký tự!')
    .max(32, 'Mật khẩu phải có 8-32 ký tự!')
    .matches(regexPassword, 'Mật khẩu gồm có chữ hoa, chữ thường và số'),
  name: yup.string().required('Tài khoản không để trống'),
  phoneNumber: yup.string
    .required('Số điện thoại không để trống')
    .matches(regexPhoneNumber, 'Số điện thoại từ 10 - 11 số'),
};
