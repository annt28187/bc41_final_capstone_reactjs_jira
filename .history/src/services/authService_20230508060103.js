import axios from 'axios';
import * as yup from 'yup';
import { yupContent } from './yupContent';
import https from './https';

export const schemaSignin = yup.object().shape({
  email: yupContent.email,
  password: yupContent.password,
});

export const schemaSignup = yup.object().shape(yupContent);

export const authServ = {
  signIn: (data) => {
    return https.post('/Users/signin', data);
  },
  signUp: (data) => {
    return https.post('/Users/signup', data);
  },
  fetchMe: (params) => {
    return https.get('/Users/getUser', { params });
  },
};
