import * as yup from 'yup';

const regexPassword = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;

export const yupContent = {
  email: yup.string().required('Email không để trống').email('Email không hợp lệ'),
  password: yup
    .string()
    .required('Mật khẩu không để trống')
    .min(8, 'Mật khẩu phải có 8-32 ký tự!')
    .max(32, 'Mật khẩu phải có 8-32 ký tự!')
    .matches(regexPassword, 'Mật khẩu gồm có chữ hoa, chữ thường, số'),
};
