import React from 'react';
import { Button, Form, Input, Modal, Typography } from 'antd';
import Swal from 'sweetalert2';
import { useFormik } from 'formik';
import { useDispatch } from 'react-redux';
import { schemaEditUser } from '../../services/userService';
import { fetchAllUsers, updateUser } from '../../../store/actions/userAction';

export default function EditUserModal({ visible, onCancel, user }) {
  const { userId: id, email, name, phoneNumber } = user;
  const dispatch = useDispatch();
  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      id,
      email,
      name,
      phoneNumber,
      password: '',
      passwordConfirmation: '',
    },
    validateOnMount: true,
    validationSchema: schemaEditUser,
  });

  const handleSubmit = () => {
    formik.setTouched({
      email: true,
      name: true,
      phoneNumber: true,
      password: true,
      passwordConfirmation: true,
    });

    if (!formik.dirty) return;

    if (!formik.isValid) return;
    dispatch(
      updateUser(formik.values, () => {
        // BE store data hơi lâu nên phải setTimeout
        setTimeout(() => {
          dispatch(fetchAllUsers());
          Swal.fire({
            title: 'User updated successfully',
            icon: 'success',
            showConfirmButton: false,
          });
          formik.resetForm();
          onCancel();
        }, 400);
      })
    );
  };
  return <div>EditUserModal</div>;
}
