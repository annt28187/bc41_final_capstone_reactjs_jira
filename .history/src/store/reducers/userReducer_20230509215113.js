import { SET_USER_LIST } from '../constants/userConstants';
import { GET_PROJECT_MEMBERS } from '../constants/taskConstants';

const initialState = {
  userList: [],
  projectMembers: [],
};

export const userReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_USER_LIST: {
      state.userList = payload;
      return { ...state };
    }
    case GET_PROJECT_MEMBERS:
      state.projectMembers = payload;
      return { ...state };

    default:
      return state;
  }
};
