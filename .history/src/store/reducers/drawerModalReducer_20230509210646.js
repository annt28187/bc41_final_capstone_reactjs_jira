import {
  HIDE_DRAWER,
  OPEN_FORM_IN_DRAWER_POPUP,
  SET_SUBMIT_FUNCTION,
} from './../constants/drawerModelConstants';

const inititalState = {
  visible: false,
  title: '',
  CompContentDrawer: <p>Default</p>,
  callBackSubmit: (propsValue) => {
    alert('click demo!');
  },
};
