import { SET_COMMENT_ERROR } from '../constants/commentConstants';

const initialState = {
  error: null,
};

export const priorityReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case GET_PRIORITY:
      state.priority = payload;
      return { ...state };
    default:
      return state;
  }
};
