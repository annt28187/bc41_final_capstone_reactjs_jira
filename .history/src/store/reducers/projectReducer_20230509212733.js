import {
  SET_PROJECT_LIST,
  SET_PROJECT_CATEGORIES,
  SET_PROJECT_ERROR,
  SET_PROJECT_MEMBERS,
  SET_PROJECT_DETAIL,
} from '../constants/projectConstants';

const initialState = {
  projectList: [],
  projectCategories: [],
  projectMembers: [],
  projectDetail: null,
  error: null,
};

const projectReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case actionType.SET_PROJECT_LIST: {
      state.projectList = payload;
      return { ...state };
    }
    case actionType.SET_PROJECT_CATEGORIES: {
      state.projectCategories = payload;
      return { ...state };
    }
    case actionType.SET_PROJECT_MEMBERS: {
      state.projectMembers = payload;
      return { ...state };
    }
    case actionType.SET_PROJECT_DETAIL: {
      state.projectDetail = payload;
      return { ...state };
    }
    case actionType.SET_PROJECT_ERROR: {
      state.error = payload;
      return { ...state };
    }
    default:
      return state;
  }
};
