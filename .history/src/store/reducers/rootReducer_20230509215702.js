import { createStore, combineReducers, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

// reducers
import { meReducer } from './meReducer';
import { commentReducer } from './commentReducer';
import { drawerModalReducer } from './drawerModalReducer';
import { priorityReducer } from './priorityReducer';
import { projectReducer } from './projectReducer';
import { statusReducer } from './statusReducer';
import { taskReducer } from './taskReducer';
import { userReducer } from './userReducer';

const rootReducer = combineReducers({
  meReducer,
  commentReducer,
  drawerModalReducer,
  priorityReducer,
  projectReducer,
  statusReducer,
  taskReducer,
  userReducer,
});
