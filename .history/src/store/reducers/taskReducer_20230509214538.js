import { SET_TASK_TYPES, SET_TASK_DETAIL, SET_TASK_ERROR } from '../constants/taskConstants';

const initialState = {
  taskTypes: [],
  taskDetail: null,
  error: null,
};

export const taskReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_TASK_TYPES: {
      state.taskTypes = payload;
      return { ...state };
    }
    case SET_TASK_DETAIL: {
      state.taskDetail = payload;
      return { ...state };
    }
    case SET_TASK_ERROR: {
      state.error = payload;
      return { ...state };
    }
    default:
      return state;
  }
};
