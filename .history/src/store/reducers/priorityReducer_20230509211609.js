import { GET_PRIORITY } from './../constants/priorityConstants';

const initialState = {
  priority: [],
};

export const priorityReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case GET_PRIORITY:
      state.priority = payload;
      return { ...state };
    default:
      return state;
  }
};
