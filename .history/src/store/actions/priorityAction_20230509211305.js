import { priorityService } from '../../services';
import { createAction } from '.';
import { GET_PRIORITY } from './../constants/priorityConstants';

export const getPriority = async (dispatch) => {
  try {
    const res = await priorityService.getPriority();

    dispatch(createAction(GET_PRIORITY, res.data.content));
  } catch (err) {
    console.log(err);
  }
};
