import swal from 'sweetalert';
import { createAction } from './createAction';
import { ACCESS_TOKEN } from './../../services/config';
import { SET_ME } from './../constants/meConstants';
import { authService } from './../../services/authService';

export const signIn = (data, callback) => {
  return async (dispatch) => {
    try {
      const res = await authService.signIn(data);
      swal('Xin chào bạn đến Jira!', 'Đăng nhập thành công!', 'success');

      dispatch(createAction(SET_ME, res.data.content));

      localStorage.setItem(ACCESS_TOKEN, res.data.content.accessToken);
      localStorage.setItem('signInInfo', JSON.stringify(res.data.content));

      if (callback) {
        callback();
      }
    } catch (err) {
      console.log(err);
      swal('Đăng nhập thất bại!', err.response.data.message, 'error');
    }
  };
};

export const signUp = (data, callback) => {
  return async (dispatch) => {
    try {
      await authService.signUp(data);

      swal('Đăng ký thành công!', 'Vui lòng tiếp tục đăng nhập!', 'success');

      if (callback) {
        callback();
      }
    } catch (err) {
      console.log(err);
      swal('Đăng ký thất bại!', err.response.data.message, 'error');
    }
  };
};

export const fetchMe = async (dispatch) => {
  try {
    const res = await authService.fetchMe();

    const loginInfo = JSON.parse(localStorage.getItem('signInInfo'));

    const me = res.data.content.find((user) => user.userId === loginInfo.id);

    if (me === undefined) {
      localStorage.removeItem(ACCESS_TOKEN);
      localStorage.removeItem('loginInfo');
      window.location.reload();
      return;
    }

    dispatch(createAction(SET_ME, { ...me, id: me.userId }));
  } catch (err) {
    console.log(err);
  }
};
