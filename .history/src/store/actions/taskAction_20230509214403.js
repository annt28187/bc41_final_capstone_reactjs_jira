import { createAction } from './createAction';
import { SET_TASK_TYPES, SET_TASK_DETAIL, SET_TASK_ERROR } from '../constants/taskConstants';
import { HIDE_DRAWER } from './../constants/drawerModelConstants';
import { notifitying } from '../../utils/notification';
import { taskService } from '../../services/taskService';

export const updateTaskStatus = ({ taskId, statusId }, callback) => {
  return async () => {
    try {
      await taskService.updateTaskStatus(taskId, statusId);

      if (callback) {
        callback();
      }
    } catch (err) {
      console.log(err);
    }
  };
};

export const createTask = (data, callback) => {
  return async (dispatch) => {
    dispatch(createAction(SET_TASK_ERROR, null));
    try {
      await taskService.createTask(data);

      if (callback) {
        callback();
      }
    } catch (err) {
      console.log(err);

      if (
        err.response.data.statusCode === 500 &&
        err.response.data.content === 'Task đã tồn tại!'
      ) {
        dispatch(createAction(SET_TASK_ERROR, 'Task đã tồn tại!'));
      }

      if (err.response.data.statusCode === 403) {
        dispatch(createAction(SET_TASK_ERROR, err.response.data.content));
      }
    }
  };
};

export const fetchAllTaskTypes = async (dispatch) => {
  try {
    const res = await taskService.fetchAllTaskTypes();

    dispatch(createAction(SET_TASK_TYPES, res.data.content));
  } catch (err) {
    console.log(err);
  }
};

export const createTaskForm = (data) => {
  return async (dispatch) => {
    try {
      const res = await taskService.createTask(data);

      console.log(res.data);

      notifitying('success', 'Task đã tạo thành công!');
      dispatch(createAction(HIDE_DRAWER));
    } catch (err) {
      console.log(err);
      notifitying('warning', 'Không thể tạo task');
      dispatch(createAction(HIDE_DRAWER));
    }
  };
};

export const fetchTaskDetail = (taskId, callback) => {
  return async (dispatch) => {
    try {
      const res = await taskService.fetchTaskDetail(taskId);

      dispatch(createAction(SET_TASK_DETAIL, res.data.content));

      if (callback) {
        callback();
      }
    } catch (err) {
      console.log(err);
    }
  };
};

export const updateTask = (data, callback) => {
  return async (dispatch) => {
    dispatch(createAction(SET_TASK_ERROR, null));
    try {
      await taskService.updateTask(data);

      if (callback) {
        callback();
      }
    } catch (err) {
      console.log(err);
      if (err.response.data.statusCode === 403) {
        dispatch(createAction(SET_TASK_ERROR, err.response.data.content));
      }
    }
  };
};

export const updateDescription = (data, callback) => {
  return async (dispatch) => {
    dispatch(createAction(SET_TASK_ERROR, null));
    try {
      await taskService.updateDescription(data);

      if (callback) {
        callback();
      }
    } catch (err) {
      console.log(err);
      if (
        err.response.data.statusCode === 404 &&
        err.response.data.content === 'Người dùng không được chỉ định!'
      ) {
        dispatch(createAction(SET_TASK_ERROR, err.response.data.content));
      }
    }
  };
};

export const updatePriority = (data, callback) => {
  return async (dispatch) => {
    dispatch(createAction(SET_TASK_ERROR, null));
    try {
      await taskService.updatePriority(data);

      if (callback) {
        callback();
      }
    } catch (err) {
      console.log(err);
      if (
        err.response.data.statusCode === 404 &&
        err.response.data.content === 'Người dùng không được chỉ định!'
      ) {
        dispatch(createAction(SET_TASK_ERROR, err.response.data.content));
      }
    }
  };
};

export const assignUserToTask = (data, callback) => {
  return async () => {
    try {
      taskService.assignUserToTask(data);

      if (callback) {
        callback();
      }
    } catch (err) {
      console.log(err);
    }
  };
};

export const removeUserFromTask = (data, callback) => {
  return async () => {
    try {
      taskService.removeUserFromTask(data);

      if (callback) {
        callback();
      }
    } catch (err) {
      console.log(err);
    }
  };
};

export const updateEstimate = (data, callback) => {
  return async (dispatch) => {
    dispatch(createAction(SET_TASK_ERROR, null));
    try {
      await taskService.updateEstimate(data);

      if (callback) {
        callback();
      }
    } catch (err) {
      console.log(err);
      if (
        err.response.data.statusCode === 404 &&
        err.response.data.content === 'Người dùng không được chỉ định!'
      ) {
        dispatch(createAction(SET_TASK_ERROR, err.response.data.content));
      }
    }
  };
};

export const updateTimeTracking = (data, callback) => {
  return async (dispatch) => {
    dispatch(createAction(SET_TASK_ERROR, null));
    try {
      await taskService.updateTimeTracking(data);

      if (callback) {
        callback();
      }
    } catch (err) {
      console.log(err);
      if (
        err.response.data.statusCode === 404 &&
        err.response.data.content === 'Người dùng không được chỉ định!'
      ) {
        dispatch(createAction(SET_TASK_ERROR, err.response.data.content));
      }
    }
  };
};

export const removeTask = (params, callback) => {
  return async (dispatch) => {
    dispatch(createAction(SET_TASK_ERROR, null));
    try {
      await taskService.removeTask(params);

      if (callback) {
        callback();
      }
    } catch (err) {
      console.log(err);
      if (err.response.data.statusCode === 403) {
        dispatch(createAction(SET_TASK_ERROR, err.response.data.content));
      }
    }
  };
};
