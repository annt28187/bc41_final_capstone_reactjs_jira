import swal from 'sweetalert';
import { createAction } from '.';
import { authService } from '../../services';
import { ACCESS_TOKEN } from '../../services/config';
import { actionType } from './type';

export const logIn = (values, callback) => {
  return async (dispatch) => {
    try {
      const res = await authService.logIn(values);
      swal('Welcome to Jira!', 'Logged in successfully!', 'success');

      dispatch(createAction(actionType.SET_ME, res.data.content));

      localStorage.setItem(ACCESS_TOKEN, res.data.content.accessToken);
      localStorage.setItem('loginInfo', JSON.stringify(res.data.content));

      if (callback) {
        callback();
      }
    } catch (err) {
      console.log(err);
      swal('Awww!', err.response.data.message, 'error');
    }
  };
};
