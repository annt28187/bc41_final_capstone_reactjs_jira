import { createAction } from './createAction';
import {
  SET_PROJECT_LIST,
  SET_PROJECT_CATEGORIES,
  SET_PROJECT_ERROR,
  SET_PROJECT_MEMBERS,
  SET_PROJECT_DETAIL,
} from '../constants/projectConstants';
import { notifitying } from '../../utils/notification';
