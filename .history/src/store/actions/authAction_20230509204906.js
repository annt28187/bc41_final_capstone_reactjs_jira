import swal from 'sweetalert';
import { createAction } from './createAction';
import { ACCESS_TOKEN } from './../../services/config';
import { SET_ME } from './../constants/meConstants';
import { authService } from './../../services/authService';

export const signIn = (values, callback) => {
  return async (dispatch) => {
    try {
      const res = await authService.signIn(values);
      swal('Xin chào bạn đến Jira!', 'Đăng nhập thành công!', 'success');

      dispatch(createAction(SET_ME, res.data.content));

      localStorage.setItem(ACCESS_TOKEN, res.data.content.accessToken);
      localStorage.setItem('signInInfo', JSON.stringify(res.data.content));

      if (callback) {
        callback();
      }
    } catch (err) {
      console.log(err);
      swal('Đăng nhập thất bài!', err.response.data.message, 'error');
    }
  };
};
