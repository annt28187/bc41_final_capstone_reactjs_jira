import React, { useEffect } from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { ACCESS_TOKEN } from './services/config';
import { fetchMe } from './redux/actions/authAction';
import { AuthRoute, PrivateRoute } from './HOCs/route/rootRoute';
import DrawerModalPopup from './HOCs/DrawerModalPopup';
import { createBrowserHistory } from 'history';
// layouts
import AuthLayout from './HOCs/layouts/AuthLayout/AuthLayout';
import MainLayout from './HOCs/layouts/MainLayout/MainLayout';
import SigninPage from './pages/SigninPage/SigninPage';
import SignupPage from './pages/SignupPage/SignupPage';
import ProjectPage from './pages/ProjectPage/ProjectPage';
import NewProjectPage from './pages/ProjectPage/NewProjectPage/NewProjectPage';
import DetailProjectPage from './pages/ProjectPage/DetailProjectPage/DetailProjectPage';
import EditProjectPage from './pages/ProjectPage/EditProjectPage/EditProjectPage';
import TaskPage from './pages/TaskPage/TaskPage';
import MyProfilePage from './pages/MyProfilePage/MyProfilePage';
import UserManagementPage from './pages/UserManagementPage/UserManagementPage';
import NotFoundPage from './pages/NotFoundPage/NotFoundPage';

// pages
export const history = createBrowserHistory();

const App = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    const token = localStorage.getItem(ACCESS_TOKEN);
    if (token) dispatch(fetchMe);
  }, [dispatch]);

  return (
    <Router history={history}>
      <DrawerModalPopup />

      <Switch>
        <AuthRoute
          path="/signin"
          exact
          component={SigninPage}
          redirectPath="/projects"
          layout={AuthLayout}
        />
        <AuthRoute path="/register" exact component={SignupPage} redirectPath="/projects" />
        <PrivateRoute
          path="/projects"
          exact
          component={ProjectPage}
          layout={MainLayout}
          redirectPath="/signin"
        />
        <PrivateRoute
          path="/projects/new"
          exact
          component={NewProjectPage}
          layout={MainLayout}
          redirectPath="/signin"
        />
        <PrivateRoute
          path="/projects/:id"
          exact
          component={DetailProjectPage}
          layout={MainLayout}
          redirectPath="/signin"
        />
        <PrivateRoute
          path="/projects/:id/edit"
          exact
          component={EditProjectPage}
          layout={MainLayout}
          redirectPath="/signin"
        />
        <PrivateRoute
          path="/projects/:projectId/board"
          exact
          component={TaskPage}
          layout={MainLayout}
          redirectPath="/signin"
        />
        <PrivateRoute
          path="/users"
          exact
          component={UserManagementPage}
          layout={MainLayout}
          redirectPath="/signin"
        />
        <PrivateRoute
          path="/my-profile"
          exact
          component={MyProfilePage}
          layout={MainLayout}
          redirectPath="/signin"
        />
        <Route path="/" exact component={SigninPage} />

        <PrivateRoute
          path="*"
          component={NotFoundPage}
          layout={MainLayout}
          redirectPath="/signin"
        />
      </Switch>
    </Router>
  );
};

export default App;
