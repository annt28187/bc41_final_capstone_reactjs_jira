export const localUserService = {
  get: (con) => {
    let jsonData = localStorage.getItem(con);
    return JSON.parse(jsonData);
  },
  set: (con, userInfo) => {
    let jsonData = JSON.stringify(userInfo);
    localStorage.setItem(con, jsonData);
  },
  remove: (con) => {
    localStorage.removeItem(con);
  },
};
