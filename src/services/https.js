import axios from 'axios';
import { BASE_URL, ACCESS_TOKEN, TOKEN_CYBERSOFT } from './config';

const https = axios.create({
  baseURL: BASE_URL,
  headers: {
    'TokenCybersoft': TOKEN_CYBERSOFT,
  },
});

https.interceptors.request.use(
  (request) => {
    const accessToken = localStorage.getItem(ACCESS_TOKEN);

    if (accessToken) {
      request.headers.Authorization = 'Bearer ' + accessToken;
    }
    return request;
  },
  (error) => {
    return Promise.reject(error);
  }
);

https.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    return Promise.reject(error);
  }
);

export default https;
