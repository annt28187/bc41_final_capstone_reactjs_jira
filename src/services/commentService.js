import https from './https';


export const commentService = {
  fetchAllComments: (params) => {
    return https.get('/Comment/getAll', { params });
  },
  insertComment: (data) => {
    return https.post('/Comment/insertComment', data);
  },
  deleteComment: (params) => {
    return https.delete('/Comment/deleteComment', { params });
  },
  updateComment: (params) => {
    return https.put('/Comment/updateComment',{}, { params });
  },
};
