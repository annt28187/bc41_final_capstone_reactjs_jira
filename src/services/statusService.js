import https from './https';


export const statusService = {
  getStatus: () => {
    return https.get('/Status/getAll');
  },
};
