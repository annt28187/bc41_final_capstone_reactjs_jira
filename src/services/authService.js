import * as yup from 'yup';
import { yupSignupContent } from './yupContent';
import https from './https';


export const schemaSignin = yup.object().shape({
  email: yupSignupContent.email,
  password: yupSignupContent.password,
});

export const schemaSignup = yup.object().shape(yupSignupContent);

export const authService = {
  signIn: (data) => {
    return https.post('/Users/signin', data);
  },
  signUp: (data) => {
    return https.post('/Users/signup', data);
  },
  fetchMe: (params) => {
    return https.get('/Users/getUser', { params });
  },
};
