import { yupEditUserContent } from './yupContent';
import https from './https';
import * as yup from 'yup';

export const schemaEditUser = yup.object().shape(yupEditUserContent);

export const userService = {
  fetchAllUsers: (params) => {
    return https.get('/Users/getUse', { params });
  },
  deleterUser: (id) => {
    return https.delete(`/Users/deleteUser?id=${id}`);
  },
  getMembersByProjectId: (projectId) => {
    return https.get(`/Users/getUserByProjectId?idProject=${projectId}`);
  },
  updateUser: (data) => {
    return https.put('/Users/editUser', data);
  },
};
