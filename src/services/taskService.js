import { yupCreateTaskContent } from './yupContent';
import https from './https';
import * as yup from 'yup';

export const schemaCreateTask = yup.object().shape(yupCreateTaskContent);

export const taskService = {
  updateTaskStatus: (taskId, statusId) => {
    return https.put('/Project/updateStatus', { taskId, statusId });
  },

  createTask: (data) => {
    return https.post('/Project/createTask', data);
  },

  fetchAllTaskTypes: () => {
    return https.get('/TaskType/getAll');
  },

  fetchTaskDetail: (taskId) => {
    return https.get('/Project/getTaskDetail', {
      params: { taskId },
    });
  },

  updateTask: (data) => {
    return https.post('/Project/updateTask', data);
  },

  updateDescription: (data) => {
    return https.put('/Project/updateDescription', data);
  },

  updatePriority: (data) => {
    return https.put('/Project/updatePriority', data);
  },

  assignUserToTask: (data) => {
    return https.post('/Project/assignUserTask', data);
  },

  removeUserFromTask: (data) => {
    return https.post('/Project/removeUserFromTask', data);
  },

  updateEstimate: (data) => {
    return https.put('/Project/updateEstimate', data);
  },

  updateTimeTracking: (data) => {
    return https.put('/Project/updateTimeTracking', data);
  },

  removeTask: (params) => {
    return https.delete('/Project/removeTask', { params });
  },
};
