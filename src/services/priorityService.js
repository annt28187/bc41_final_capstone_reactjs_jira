import https from './https';


export const priorityService = {
  getPriority: () => {
    return https.get('/Priority/getAll');
  },
};
