import React from 'react';
import { Typography } from 'antd';

export default function DetailProjectPage() {
  return (
    <Typography.Title level={3} className="my-6">
      Project detail page goes here
    </Typography.Title>
  );
}
