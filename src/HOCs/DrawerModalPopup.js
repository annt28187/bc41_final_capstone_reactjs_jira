import React from 'react';
import { Drawer, Button, Space } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { createAction } from '../redux/actions/createAction';
import { HIDE_DRAWER } from '../redux/constants/drawerModelConstants';

export default function DrawerModalPopup(props) {
  const dispatch = useDispatch();
  const { visible, CompContentDrawer, callBackSubmit, title } = useSelector(
    (state) => state.drawerModalReducer
  );

  const onClose = () => {
    dispatch(createAction(HIDE_DRAWER));
  };

  return (
    <Drawer
      title={title}
      width={720}
      onClose={onClose}
      open={visible}
      zIndex={1050}
      footer={
        <Space>
          <Button onClick={onClose}>Cancel</Button>
          <Button onClick={callBackSubmit} type="primary">
            Submit
          </Button>
        </Space>
      }
    >
      {CompContentDrawer}
    </Drawer>
  );
}
