import MyProfilePage from '../../pages/MyProfilePage/MyProfilePage';
import NotFoundPage from '../../pages/NotFoundPage/NotFoundPage';
import DetailProjectPage from '../../pages/ProjectPage/DetailProjectPage/DetailProjectPage';
import EditProjectPage from '../../pages/ProjectPage/EditProjectPage/EditProjectPage';
import NewProjectPage from '../../pages/ProjectPage/NewProjectPage/NewProjectPage';
import ProjectPage from '../../pages/ProjectPage/ProjectPage';
import SigninPage from '../../pages/SigninPage/SigninPage';
import SignupPage from '../../pages/SignupPage/SignupPage';
import TaskPage from '../../pages/TaskPage/TaskPage';
import UserManagementPage from '../../pages/UserManagementPage/UserManagementPage';
import AuthLayout from '../layouts/AuthLayout/AuthLayout';
import MainLayout from '../layouts/MainLayout/MainLayout';

export const rootRoute = [
  { url: '/', component: <AuthLayout Component={SigninPage} /> },
  { url: '/signin', component: <AuthLayout Component={SigninPage} /> },
  { url: '/siginup', component: <AuthLayout Component={SignupPage} /> },
  {
    url: '/projects',
    component: <MainLayout Component={ProjectPage} />,
  },
  {
    url: '/projects/new',
    component: <MainLayout Component={NewProjectPage} />,
  },
  {
    url: '/projects/:id',
    component: <MainLayout Component={DetailProjectPage} />,
  },
  {
    url: '/projects/:id/edit',
    component: <MainLayout Component={EditProjectPage} />,
  },
  {
    url: '/projects/:projectId/board',
    component: <MainLayout Component={TaskPage} />,
  },
  {
    url: '/users',
    component: <MainLayout Component={UserManagementPage} />,
  },
  {
    url: '/my-profile',
    component: <MainLayout Component={MyProfilePage} />,
  },
  {
    url: '*',
    component: <NotFoundPage />,
  },
];
