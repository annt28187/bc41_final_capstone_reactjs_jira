import { SET_COMMENT_ERROR } from './../constants/commentConstants';

const initialState = {
  error: null,
};

export const commentReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_COMMENT_ERROR: {
      state.error = payload;
      return { ...state };
    }
    default:
      return state;
  }
};
