import { SET_ME } from './../constants/meConstants';

const initialState = null;

export const meReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_ME: {
      state = payload;
      return state;
    }
    default:
      return state;
  }
};
