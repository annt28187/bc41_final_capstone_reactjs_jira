import { GET_STATUS } from '../constants/statusConstants';

const intitialValue = {
  statusTypes: [],
};

export const statusReducer = (state = intitialValue, { type, payload }) => {
  switch (type) {
    case GET_STATUS:
      state.statusTypes = payload;
      return { ...state };
    default:
      return state;
  }
};
