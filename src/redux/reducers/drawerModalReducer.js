import {
  HIDE_DRAWER,
  OPEN_FORM_IN_DRAWER_POPUP,
  SET_SUBMIT_FUNCTION,
} from './../constants/drawerModelConstants';

const inititalState = {
  visible: false,
  title: '',
  CompContentDrawer: <p>Default</p>,
  callBackSubmit: (propsValue) => {
    alert('Nhấn vào đây!');
  },
};

export const drawerModalReducer = (state = inititalState, { type, payload }) => {
  switch (type) {
    case HIDE_DRAWER:
      return { ...state, visible: false };
    case OPEN_FORM_IN_DRAWER_POPUP:
      return {
        ...state,
        visible: true,
        CompContentDrawer: payload.component,
        title: payload.title,
      };
    case SET_SUBMIT_FUNCTION:
      return { ...state, callBackSubmit: payload };
    default:
      return state;
  }
};
