import { createAction } from './createAction';
import { GET_PRIORITY } from './../constants/priorityConstants';
import { priorityService } from './../../services/priorityService';

export const getPriority = async (dispatch) => {
  try {
    const res = await priorityService.getPriority();

    dispatch(createAction(GET_PRIORITY, res.data.content));
  } catch (err) {
    console.log(err);
  }
};
