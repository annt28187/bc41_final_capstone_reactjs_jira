import { statusService } from './../../services/statusService';
import { createAction } from './createAction';
import { GET_STATUS } from '../constants/statusConstants';

export const getStatus = async (dispatch) => {
  try {
    const res = await statusService.getStatus();

    dispatch(createAction(GET_STATUS, res.data.content));
  } catch (err) {
    console.log(err);
  }
};
