import { createAction } from './createAction';
import { notifitying } from '../../utils/notification';
import { userService } from '../../services/userService';
import { SET_USER_LIST } from '../constants/userConstants';
import { GET_PROJECT_MEMBERS } from '../constants/taskConstants';

export const fetchAllUsers = (params) => {
  return async (dispatch) => {
    try {
      const res = await userService.fetchAllUsers(params);

      dispatch(createAction(SET_USER_LIST, res.data.content));
    } catch (err) {
      console.log(err);
    }
  };
};

export const deleteUser = (id) => {
  return async (dispatch) => {
    try {
      await userService.deleterUser(id);

      dispatch(fetchAllUsers());

      notifitying('success', 'Đã xóa người dùng thành công!');
    } catch (err) {
      console.log(err);
      notifitying('warning', 'Đã xóa người dùng thất bại!');
    }
  };
};

export const getMembersByProjectId = (id) => {
  return async (dispatch) => {
    try {
      const res = await userService.getMembersByProjectId(id);

      dispatch(createAction(GET_PROJECT_MEMBERS, res.data.content));
    } catch (err) {
      dispatch(createAction(GET_PROJECT_MEMBERS, []));
      console.log(err);
    }
  };
};

export const updateUser = (data, callback) => {
  return async () => {
    try {
      userService.updateUser(data);

      if (callback) {
        callback();
      }
    } catch (err) {
      console.log(err);
    }
  };
};
